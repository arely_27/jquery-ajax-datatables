$(document).ready(function(){

var getUserCall={
  url:'http://jsonplaceholder.typicode.com/posts',
  dataType:'json',
  method:'GET'
};

function printUsers(users){
  console.log(users);
  $('#dataTable').dataTable({
    data: users,
    columns:[
      {
        title:'ID',
        data:'id'
      },
      {
        title:'Title',
        data:'title'
      },
      {
        title:'Body',
        data:'body'
      }
    ],
  });
}

function logearMensaje(mensaje){
  console.log('Error',mensaje);
}

function notificarUsuario(){
  alert('The request is complete!');
}

$.ajax(getUserCall)
.done(printUsers)
.fail(logearMensaje)
.always(notificarUsuario)

});
